package com.ftb.test.ftb_test.data.models

data class PredictionBase(val matchHash: Int, val predictedScore1: Int, val predictedScore2: Int)
