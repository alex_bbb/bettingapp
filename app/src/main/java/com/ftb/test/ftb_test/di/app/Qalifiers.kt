package com.ftb.test.ftb_test.di.app

import javax.inject.Qualifier

@Qualifier
annotation class ApplicationScope

@Qualifier
annotation class ApplicationContext
