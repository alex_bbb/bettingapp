package com.ftb.test.ftb_test.data.models

class Constants {
    companion object {
        val NO_SCORE = -1
        val UPDATE_INTERVAL = 60 * 1000;
    }
}